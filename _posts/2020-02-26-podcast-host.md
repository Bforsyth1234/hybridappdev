---
title: Podcast Host
body_of_post: ''
date: 2020-02-24 23:00:00 +0000

---
Hey Everyone!

Just wanted to let you know i'm a host on two of my favorite podcasts, The Freelancers Show and Adventures in Angular.

* [https://devchat.tv/freelancers/](https://devchat.tv/freelancers/ "https://devchat.tv/freelancers/")
* [https://devchat.tv/adv-in-angular/](https://devchat.tv/adv-in-angular/ "https://devchat.tv/adv-in-angular/")