---
layout: page
title: About
permalink: "/about/"

---
Brooks Forsyth is a hybrid mobile app developer living with his wife and two kids in Connecticut. When not coding Brooks enjoys spending time with his family and growing coral in his salt water aquarium.

![](/uploads/FullSizeRender.jpg)